/******************************************************************
   set pins connected to PS2 controller:
     - 1e column: original
     - 2e colmun: Stef?
   replace pin numbers by the ones you use
 ******************************************************************/
#define PS2_DAT        52  //14    
#define PS2_CMD        50  //15
#define PS2_SEL        48  //16
#define PS2_CLK        46  //17

#include <PS2X_lib.h>  //for v1.6

/******************************************************************
   select modes of PS2 controller:
     - pressures = analog reading of push-butttons
     - rumble    = motor rumbling
   uncomment 1 of the lines for each mode selection
 ******************************************************************/
//#define pressures   true
#define pressures   false
//#define rumble      true
#define rumble      false

PS2X ps2x; // create PS2 Controller Class

//right now, the library does NOT support hot pluggable controllers, meaning
//you must always either restart your Arduino after you connect the controller,
//or call config_gamepad(pins) again after connecting the controller.

int error = 0;
byte type = 0;
byte vibrate = 0;


/////////////////////// Motor Pin Definition /////////////////////////////
//// M3 Pin -> Working Okay
int M1_RPWM = 7;
int M1_LPWM = 6;
int M2_RPWM = 8;
int M2_LPWM = 9;
int M3_RPWM = 11;
int M3_LPWM = 10;
int M4_RPWM = 12;
int M4_LPWM = 13;



/////////////////////// Motor Speed Definition ///////////////////////////
int maxSpeed = 255; //(0:Stop ~ 255:FullSpeed)

void setup() {

  //////////////////// Motor Pin Mode Setup ///////////////////////////////
  pinMode(M1_RPWM, OUTPUT);
  pinMode(M1_LPWM, OUTPUT);
  pinMode(M2_RPWM, OUTPUT);
  pinMode(M2_LPWM, OUTPUT);
  pinMode(M3_RPWM, OUTPUT);
  pinMode(M3_LPWM, OUTPUT);
  pinMode(M4_RPWM, OUTPUT);
  pinMode(M4_LPWM, OUTPUT);




  Serial.begin(57600);

  delay(300);  //added delay to give wireless ps2 module some time to startup, before configuring it

  //CHANGES for v1.6 HERE!!! **************PAY ATTENTION*************

  //setup pins and settings: GamePad(clock, command, attention, data, Pressures?, Rumble?) check for error
  error = ps2x.config_gamepad(pressures, rumble);

  if (error == 0) {
    Serial.print("Found Controller, configured successful ");
    Serial.print("pressures = ");
    if (pressures)
      Serial.println("true ");
    else
      Serial.println("false");
    Serial.print("rumble = ");
    if (rumble)
      Serial.println("true)");
    else
      Serial.println("false");
    Serial.println("Try out all the buttons, X will vibrate the controller, faster as you press harder;");
    Serial.println("holding L1 or R1 will print out the analog stick values.");
    Serial.println("Note: Go to www.billporter.info for updates and to report bugs.");
  }
  else if (error == 1)
    Serial.println("No controller found, check wiring, see readme.txt to enable debug. visit www.billporter.info for troubleshooting tips");

  else if (error == 2)
    Serial.println("Controller found but not accepting commands. see readme.txt to enable debug. Visit www.billporter.info for troubleshooting tips");

  else if (error == 3)
    Serial.println("Controller refusing to enter Pressures mode, may not support it. ");

  //  Serial.print(ps2x.Analog(1), HEX);

  type = ps2x.readType();
  switch (type) {
    case 0:
      Serial.print("Unknown Controller type found ");
      break;
    case 1:
      Serial.print("DualShock Controller found ");
      break;
    case 2:
      Serial.print("GuitarHero Controller found ");
      break;
    case 3:
      Serial.print("Wireless Sony DualShock Controller found ");
      break;
  }

  // moveForward();
  // delay(2000);

  //motorStop();
}

void loop() {
  /* You must Read Gamepad to get new values and set vibration values
     ps2x.read_gamepad(small motor on/off, larger motor strenght from 0-255)
     if you don't enable the rumble, use ps2x.read_gamepad(); with no values
     You should call this at least once a second
  */
  //if(error == 1) //skip loop if no controller found
  //return;

  /*if(type == 2){ //Guitar Hero Controller
    ps2x.read_gamepad();          //read controller

    if(ps2x.ButtonPressed(GREEN_FRET))
      Serial.println("Green Fret Pressed");
    if(ps2x.ButtonPressed(RED_FRET))
      Serial.println("Red Fret Pressed");
    if(ps2x.ButtonPressed(YELLOW_FRET))
      Serial.println("Yellow Fret Pressed");
    if(ps2x.ButtonPressed(BLUE_FRET))
      Serial.println("Blue Fret Pressed");
    if(ps2x.ButtonPressed(ORANGE_FRET))
      Serial.println("Orange Fret Pressed");

    if(ps2x.ButtonPressed(STAR_POWER))
      Serial.println("Star Power Command");

    if(ps2x.Button(UP_STRUM))          //will be TRUE as long as button is pressed
      Serial.println("Up Strum");
    if(ps2x.Button(DOWN_STRUM))
      Serial.println("DOWN Strum");

    if(ps2x.Button(PSB_START))         //will be TRUE as long as button is pressed
      Serial.println("Start is being held");
    if(ps2x.Button(PSB_SELECT))
      Serial.println("Select is being held");

    if(ps2x.Button(ORANGE_FRET)) {     // print stick value IF TRUE
      Serial.print("Wammy Bar Position:");
      Serial.println(ps2x.Analog(WHAMMY_BAR), DEC);
    }
    }*/
  //else { //DualShock Controller

  ps2x.read_gamepad(false, vibrate); //read controller and set large motor to spin at 'vibrate' speed

  if (ps2x.Button(PSB_START))        //will be TRUE as long as button is pressed
    Serial.println("Start is being held");
  if (ps2x.Button(PSB_SELECT))
    Serial.println("Select is being held");

  //////////////////////////////////// Mecanum Four Wheel Movement Control //////////////////////////////////////////
  if (ps2x.Button(PSB_PAD_UP)) {     //will be TRUE as long as button is pressed
    //Serial.print("Up held this hard: ");
    //Serial.println(ps2x.Analog(PSAB_PAD_UP), DEC);
    moveForward();
  }
  else if (ps2x.Button(PSB_PAD_DOWN)) {
    Serial.print("DOWN held this hard: ");
    Serial.println(ps2x.Analog(PSAB_PAD_DOWN), DEC);
    moveBackward();
  }
  else if (ps2x.Button(PSB_PAD_RIGHT)) {
    //Serial.print("Right held this hard: ");
    //Serial.println(ps2x.Analog(PSAB_PAD_RIGHT), DEC);
    moveRight();
  }
  else if (ps2x.Button(PSB_PAD_LEFT)) {
    //Serial.print("LEFT held this hard: ");
    //Serial.println(ps2x.Analog(PSAB_PAD_LEFT), DEC);
    moveLeft();
  }
  else if (ps2x.Button(PSB_R1))
  {
    rotateRight();
  }
  else if (ps2x.Button(PSB_L1))
  {
    rotateLeft();
  }
  else if (ps2x.Button(PSB_TRIANGLE))
  {
    move45();
  }
  else if (ps2x.Button(PSB_CIRCLE))
  {
    move135();
  }
  else if (ps2x.Button(PSB_CROSS))
  {
    move225();
  }
  else if (ps2x.Button(PSB_SQUARE))
  {
    move315();
  }
  else // press nothing -> stop the motor
  {
    motorStop();
  }

  if (ps2x.Button(PSB_SELECT)) // rotate kick mechanism
  {
    kickOn();
  }
  else if (ps2x.ButtonReleased(PSB_SELECT))
  {
    kickOff();
  }

  /*vibrate = ps2x.Analog(PSAB_CROSS);  //this will set the large motor vibrate speed based on how hard you press the blue (X) button
    if (ps2x.NewButtonState()) {        //will be TRUE if any button changes state (on to off, or off to on)
    if(ps2x.Button(PSB_L3))
      Serial.println("L3 pressed");
    if(ps2x.Button(PSB_R3))
      Serial.println("R3 pressed");
    if(ps2x.Button(PSB_L2))
      Serial.println("L2 pressed");
    if(ps2x.Button(PSB_R2))
      Serial.println("R2 pressed");
    if(ps2x.Button(PSB_TRIANGLE))
      Serial.println("Triangle pressed");
    }*/

  /*if(ps2x.ButtonPressed(PSB_CIRCLE))               //will be TRUE if button was JUST pressed
    Serial.println("Circle just pressed");
    if(ps2x.NewButtonState(PSB_CROSS))               //will be TRUE if button was JUST pressed OR released
    Serial.println("X just changed");
    if(ps2x.ButtonReleased(PSB_SQUARE))              //will be TRUE if button was JUST released
    Serial.println("Square just released");
  */

  /*if(ps2x.Button(PSB_L1) || ps2x.Button(PSB_R1)) { //print stick values if either is TRUE
    Serial.print("Stick Values:");
    Serial.print(ps2x.Analog(PSS_LY), DEC); //Left stick, Y axis. Other options: LX, RY, RX
    Serial.print(",");
    Serial.print(ps2x.Analog(PSS_LX), DEC);
    Serial.print(",");
    Serial.print(ps2x.Analog(PSS_RY), DEC);
    Serial.print(",");
    Serial.println(ps2x.Analog(PSS_RX), DEC);
    }*/
  //}
  delay(50);
}


//////////////////////////////////Four Wheel  Motor Functions ////////////////////////////////
void moveForward()
{
  Serial.println("Move Forward ");
  analogWrite(M1_RPWM, maxSpeed);
  analogWrite(M1_LPWM, 0);
  analogWrite(M2_RPWM, maxSpeed);
  analogWrite(M2_LPWM, 0);
  analogWrite(M3_RPWM, maxSpeed);
  analogWrite(M3_LPWM, 0);
  analogWrite(M4_RPWM, maxSpeed);
  analogWrite(M4_LPWM, 0);
}
void moveBackward()
{
  Serial.println("Move Backward ");
  analogWrite(M1_RPWM, 0);
  analogWrite(M1_LPWM, maxSpeed);
  analogWrite(M2_RPWM, 0);
  analogWrite(M2_LPWM, maxSpeed);
  analogWrite(M3_RPWM, 0);
  analogWrite(M3_LPWM, maxSpeed);
  analogWrite(M4_RPWM, 0);
  analogWrite(M4_LPWM, maxSpeed);
}

void moveRight()
{
  Serial.println("Move Right");
  analogWrite(M1_RPWM, 0);
  analogWrite(M1_LPWM, maxSpeed);
  analogWrite(M2_RPWM, maxSpeed);
  analogWrite(M2_LPWM, 0);
  analogWrite(M3_RPWM, maxSpeed);
  analogWrite(M3_LPWM, 0);
  analogWrite(M4_RPWM, maxSpeed);
  analogWrite(M4_LPWM, 0);
}

void moveLeft()
{
  Serial.println("Move Left");
  analogWrite(M1_RPWM, maxSpeed);
  analogWrite(M1_LPWM, 0);
  analogWrite(M2_RPWM, 0);
  analogWrite(M2_LPWM, maxSpeed);
  analogWrite(M3_RPWM, 0);
  analogWrite(M3_LPWM, maxSpeed);
  analogWrite(M4_RPWM, maxSpeed);
  analogWrite(M4_LPWM, 0);
}

void rotateRight()
{
  Serial.println("Rotate Right");
  analogWrite(M1_RPWM, maxSpeed);
  analogWrite(M1_LPWM, 0);
  analogWrite(M2_RPWM, 0);
  analogWrite(M2_LPWM, maxSpeed);
  analogWrite(M3_RPWM, maxSpeed);
  analogWrite(M3_LPWM,0);
  analogWrite(M4_RPWM, 0);
  analogWrite(M4_LPWM, maxSpeed);
}




void rotateLeft()
{
  Serial.println("Rotate Left");
  analogWrite(M1_RPWM, 0);
  analogWrite(M1_LPWM, maxSpeed);
  analogWrite(M2_RPWM, maxSpeed);
  analogWrite(M2_LPWM, 0);
  analogWrite(M3_RPWM, 0);
  analogWrite(M3_LPWM,maxSpeed);
  analogWrite(M4_RPWM, maxSpeed);
  analogWrite(M4_LPWM, 0);
}

void move45()
{
  Serial.println("Move 45 Deg");
   analogWrite(M1_RPWM, 0);
  analogWrite(M1_LPWM, 0);
  analogWrite(M2_RPWM, maxSpeed);
  analogWrite(M2_LPWM, 0);
  analogWrite(M3_RPWM, maxSpeed);
  analogWrite(M3_LPWM,0);
  analogWrite(M4_RPWM, 0);
  analogWrite(M4_LPWM, 0);
}

void move135()
{
  Serial.println("Move 135 Deg");
  analogWrite(M1_RPWM, maxSpeed);
  analogWrite(M1_LPWM, 0);
  analogWrite(M2_RPWM, 0);
  analogWrite(M2_LPWM, 0);
  analogWrite(M3_RPWM, 0);
  analogWrite(M3_LPWM,0);
  analogWrite(M4_RPWM, maxSpeed);
  analogWrite(M4_LPWM, 0);
}

void move225()
{
  Serial.println("Move 225 Deg");
analogWrite(M1_RPWM, 0);
  analogWrite(M1_LPWM, 0);
  analogWrite(M2_RPWM, 0);
  analogWrite(M2_LPWM, maxSpeed);
  analogWrite(M3_RPWM, 0);
  analogWrite(M3_LPWM,maxSpeed);
  analogWrite(M4_RPWM, 0);
  analogWrite(M4_LPWM, 0);
}

void move315()
{
  Serial.println("Move 315 Deg");
  analogWrite(M1_RPWM, 0);
  analogWrite(M1_LPWM, maxSpeed);
  analogWrite(M2_RPWM, 0);
  analogWrite(M2_LPWM, 0);
  analogWrite(M3_RPWM, 0);
  analogWrite(M3_LPWM,0);
  analogWrite(M4_RPWM, 0);
  analogWrite(M4_LPWM, maxSpeed);
}

void motorStop()
{
  digitalWrite(M1_RPWM, 0);
  digitalWrite(M1_LPWM, 0);
  digitalWrite(M2_RPWM, 0);
  digitalWrite(M2_LPWM, 0);
  digitalWrite(M3_RPWM, 0);
  digitalWrite(M3_LPWM, 0);
  digitalWrite(M4_RPWM, 0);
  digitalWrite(M4_LPWM, 0);
}


//////////////////////////////// Kick Motor Functions ///////////////////////////////////////
void kickOn()
{
  Serial.println("Kick Mechanism On");
  //analogWrite(PWM_OUTUP1, maxSpeed);
  //analogWrite(PWM_OUPUT2, 0);
}

void kickOff()
{
  Serial.println("Kick Mechanism Off");
  //analogWrite(PWM_OUTUP1, 0);
  //analogWrite(PWM_OUPUT2, 0);
}
