/*
  IBT-2 Motor Control Board driven by Arduino.

  Speed and direction controlled by a potentiometer attached to analog input 0.
  One side pin of the potentiometer (either one) to ground; the other side pin to +5V

  Connection to the IBT-2 board:
  IBT-2 pin 1 (RPWM) to Arduino pin 5(PWM)
  IBT-2 pin 2 (LPWM) to Arduino pin 6(PWM)
  IBT-2 pins 3 (R_EN), 4 (L_EN), 7 (VCC) to Arduino 5V pin
  IBT-2 pin 8 (GND) to Arduino GND
  IBT-2 pins 5 (R_IS) and 6 (L_IS) not connected
*/

int SENSOR_PIN = 0; // center pin of the potentiometer

// GEE Board
// M1 Pin -> Working Okay
//int RPWM_Output = 2;
//int LPWM_Output = 3;

// M2 Pin -> Working Okay
int RPWM_Output = 5;
int LPWM_Output = 4;

//// M3 Pin -> Working Okay
//int RPWM_Output = 7;
//int LPWM_Output = 6;

// M4 Pin -> Working Okay
//int RPWM_Output = 8;
//int LPWM_Output = 9;

// M5 Pin -> Working Okay
//int RPWM_Output = 10;
//int LPWM_Output = 11;

//// M6 Pin -> Working Okay
//int RPWM_Output = 13;
//int LPWM_Output = 12;

int sensorValue;

void setup()
{
  pinMode(RPWM_Output, OUTPUT);
  pinMode(LPWM_Output, OUTPUT);

  Serial.begin(9600);

  int sensorValue = 100;
  
}

void loop()
{

  //int sensorValue = analogRead(SENSOR_PIN);
  Serial.println(sensorValue);

  // sensor value is in the range 0 to 1023
  // the lower half of it we use for reverse rotation; the upper half for forward rotation
  if (sensorValue < 512)
  {
    // reverse rotation
    int reversePWM = -(sensorValue - 511) / 2;
    analogWrite(LPWM_Output, 0);
    analogWrite(RPWM_Output, reversePWM);
  }
  else
  {
    // forward rotation
    int forwardPWM = (sensorValue - 512) / 2;
    analogWrite(LPWM_Output, forwardPWM);
    analogWrite(RPWM_Output, 0);
  }
}
